# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 2.0.0
### Changed
- Dropped mandrill support

## 1.0.5
### Added
- support for email attachments 

## 1.0.4
### Fixed
- Empty subject is now allowed

## 1.0.3
### Added
- Support for email subject

## 1.0.2
### Added
- Support for integer player IDs

## 1.0.1
### Added
- Language support

## 1.0.0
### Added
- Added unit tests
- Added email validations
