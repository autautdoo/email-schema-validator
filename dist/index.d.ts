export declare type MergeVar = {
    name: string;
    content: string;
};
export declare type EmailAttachment = {
    content: string;
    filename: string;
    type: string;
};
export declare type EmailData = {
    name?: string;
    email: string;
};
export interface IEmailPayload {
    to?: EmailData | EmailData[];
    playerId?: string | number;
    from?: {
        email?: string;
        name?: string;
    };
    template?: string;
    templateName?: string;
    subject?: string;
    language?: string;
    content?: string;
    globalMergeVars?: MergeVar[];
    cc?: string | string[];
    bcc?: string | string[];
    replyTo?: string;
    attachments?: EmailAttachment[];
}
export declare function validateEmailPayload(payload: IEmailPayload): Promise<any>;
