"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateEmailPayload = void 0;
const joi_1 = __importDefault(require("joi"));
const contactSchema = joi_1.default.object({
    name: joi_1.default.string(),
    email: joi_1.default.string().email({ tlds: { allow: false } }).required(),
});
const recipientSchema = joi_1.default.alternatives().try(contactSchema, joi_1.default.array().items(contactSchema));
const emailAttachmentSchema = joi_1.default.object({
    content: joi_1.default.string().base64().required(),
    filename: joi_1.default.string().required(),
    type: joi_1.default.string().required(),
});
const playerIdSchema = joi_1.default.alternatives().try(joi_1.default.string(), joi_1.default.number().cast('string')).required();
/**
     If we verify that only name and content are being sent from other services
    then we can add schema below to globalMergeVars.

    const globalMergeVarsSchema = Joi.array().items(Joi.object({
      name: Joi.string().required(),
      content: Joi.string().required(),
    }));
 **/
const baseSchema = joi_1.default.object().keys({
    subject: joi_1.default.string().allow('', null),
    globalMergeVars: joi_1.default.array(),
    language: joi_1.default.string(),
    cc: joi_1.default.string().email({ tlds: { allow: false } }),
    bcc: joi_1.default.string().email({ tlds: { allow: false } }),
    replyTo: joi_1.default.string().email({ tlds: { allow: false } }),
    attachments: joi_1.default.array().items(emailAttachmentSchema),
});
const emailBaseSchema = baseSchema.keys({
    to: recipientSchema.required(),
    from: contactSchema.required(),
    subject: joi_1.default.string().required(),
    content: joi_1.default.string().required(),
});
const playerEmailTemplateSchema = baseSchema.keys({
    playerId: playerIdSchema,
    template: joi_1.default.string().required(),
    from: contactSchema,
    to: recipientSchema,
});
const playerEmailTemplateNameSchema = baseSchema.keys({
    playerId: playerIdSchema,
    templateName: joi_1.default.string().required(),
    from: contactSchema,
    to: recipientSchema,
});
const playerEmailNoTemplateSchema = baseSchema.keys({
    playerId: playerIdSchema,
    subject: joi_1.default.string().required(),
    content: joi_1.default.string().required(),
    from: contactSchema,
    to: recipientSchema,
});
async function validateEmailPayload(payload) {
    if (!payload.playerId) {
        return await emailBaseSchema.validateAsync(payload);
    }
    else {
        if (payload.template) {
            return await playerEmailTemplateSchema.validateAsync(payload);
        }
        else if (payload.templateName) {
            return await playerEmailTemplateNameSchema.validateAsync(payload);
        }
        else {
            return await playerEmailNoTemplateSchema.validateAsync(payload);
        }
    }
}
exports.validateEmailPayload = validateEmailPayload;
//# sourceMappingURL=index.js.map