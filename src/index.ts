import Joi from 'joi';

export type MergeVar = {
    name: string,
    content: string,
}

export type EmailAttachment = {
    content: string,
    filename: string,
    type: string,
}

export type EmailData = { name?: string; email: string; }

export interface IEmailPayload {
    to?: EmailData|EmailData[],
    playerId?: string | number,
    from?: {email?: string, name?: string},
    template?: string,
    templateName?: string,
    subject?: string,
    language?: string,
    content?: string,
    globalMergeVars?: MergeVar[],
    cc?: string|string[],
    bcc?: string|string[],
    replyTo?: string,
    attachments?: EmailAttachment[],
}

const contactSchema = Joi.object({
  name: Joi.string(),
  email: Joi.string().email({tlds: {allow: false}}).required(),
});

const recipientSchema = Joi.alternatives().try(
    contactSchema,
    Joi.array().items(contactSchema),
);

const emailAttachmentSchema = Joi.object({
  content: Joi.string().base64().required(),
  filename: Joi.string().required(),
  type: Joi.string().required(),
});

const playerIdSchema = Joi.alternatives().try(
    Joi.string(), Joi.number().cast('string'),
).required();

/**
     If we verify that only name and content are being sent from other services
    then we can add schema below to globalMergeVars.

    const globalMergeVarsSchema = Joi.array().items(Joi.object({
      name: Joi.string().required(),
      content: Joi.string().required(),
    }));
 **/

const baseSchema = Joi.object().keys({
  subject: Joi.string().allow('', null),
  globalMergeVars: Joi.array(),
  language: Joi.string(),
  cc: Joi.string().email({tlds: {allow: false}}),
  bcc: Joi.string().email({tlds: {allow: false}}),
  replyTo: Joi.string().email({tlds: {allow: false}}),
  attachments: Joi.array().items(emailAttachmentSchema),
});

const emailBaseSchema = baseSchema.keys({
  to: recipientSchema.required(),
  from: contactSchema.required(),
  subject: Joi.string().required(),
  content: Joi.string().required(),
});

const playerEmailTemplateSchema = baseSchema.keys({
  playerId: playerIdSchema,
  template: Joi.string().required(),
  from: contactSchema,
  to: recipientSchema,
});

const playerEmailTemplateNameSchema = baseSchema.keys({
  playerId: playerIdSchema,
  templateName: Joi.string().required(),
  from: contactSchema,
  to: recipientSchema,
});

const playerEmailNoTemplateSchema = baseSchema.keys({
  playerId: playerIdSchema,
  subject: Joi.string().required(),
  content: Joi.string().required(),
  from: contactSchema,
  to: recipientSchema,
});

export async function validateEmailPayload(payload: IEmailPayload) {
  if (!payload.playerId) {
    return await emailBaseSchema.validateAsync(payload);
  } else {
    if (payload.template) {
      return await playerEmailTemplateSchema.validateAsync(payload);
    } else if (payload.templateName) {
      return await playerEmailTemplateNameSchema.validateAsync(payload);
    } else {
      return await playerEmailNoTemplateSchema.validateAsync(payload);
    }
  }
}
