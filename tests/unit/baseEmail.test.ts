import {validateEmailPayload} from '../../src';
import {ValidationError} from 'joi';
import {IEmailPayload} from '../../src';

type TestCasePayload = {
  name: string,
  payload: IEmailPayload,
  result: any
}

const testCases: TestCasePayload[] = [{
  name: 'should pass since all required parameters are provided',
  payload: {
    to: {name: 'Test', email: 'test@test.com'},
    from: {name: 'Test', email: 'test@test.com'},
    subject: 'test',
    content: '<h1>TEST</h1>',
    language: 'en',
    cc: 'test@test.com',
    bcc: 'test@test.com',
    replyTo: 'test@test.com',
  },
  result: {error: null},
}, {
  name: 'should pass with `to` as an array',
  payload: {
    to: [{name: 'Test', email: 'test@test.com'}],
    from: {name: 'Test', email: 'test@test.com'},
    subject: 'test',
    content: '<h1>TEST</h1>',
    language: 'en',
    cc: 'test@test.com',
    bcc: 'test@test.com',
    replyTo: 'test@test.com',
  },
  result: {error: null},
}, {
  name: 'should pass since all required parameters are provided',
  payload: {
    to: {name: 'Test', email: 'test@test.com'},
    from: {name: 'Test', email: 'test@test.com'},
    subject: 'test',
    content: '<h1>TEST</h1>',
    language: 'en',
    cc: 'test@test.com',
    bcc: 'test@test.com',
    replyTo: 'test@test.com',
    attachments: [{
      content: 'cGF2bGU=',
      filename: 'test.png',
      type: 'image/png',
    }],
  },
  result: {error: null},
}, {
  name: 'should fail due to missing subject',
  payload: {
    to: {name: 'Test', email: 'test@test.com'},
    from: {name: 'Test', email: 'test@test.com'},
    content: '<h1>TEST</h1>',
  },
  result: {error: ValidationError},
}, {
  name: 'should fail due to missing from email',
  payload: {
    to: {name: 'Test', email: 'test@test.com'},
    from: {name: 'Test'},
    subject: 'test',
    content: '<h1>TEST</h1>',
  },
  result: {error: ValidationError},
}, {
  name: 'should fail due to missing content',
  payload: {
    to: {name: 'Test', email: 'test@test.com'},
    from: {name: 'Test', email: 'test@test.com'},
    subject: 'test',
  },
  result: {error: ValidationError},
}, {
  name: 'should fail due to invalid from email',
  payload: {
    to: {name: 'Test', email: 'test@test.com'},
    from: {name: 'Test', email: 'test-test.com'},
    subject: 'test',
    content: '<h1>TEST</h1>',
  },
  result: {error: ValidationError},
}, {
  name: 'should fail due to invalid cc email',
  payload: {
    to: {name: 'Test', email: 'test@test.com'},
    from: {name: 'Test', email: 'test@test.com'},
    subject: 'test',
    content: '<h1>TEST</h1>',
    cc: 'test',
  },
  result: {error: ValidationError},
}, {
  name: 'should fail due to invalid bcc email',
  payload: {
    to: {name: 'Test', email: 'test@test.com'},
    from: {name: 'Test', email: 'test@test.com'},
    subject: 'test',
    content: '<h1>TEST</h1>',
    bcc: 'test',
  },
  result: {error: ValidationError},
}, {
  name: 'should fail due to invalid replyTo email',
  payload: {
    to: {name: 'Test', email: 'test@test.com'},
    from: {name: 'Test', email: 'test@test.com'},
    subject: 'test',
    content: '<h1>TEST</h1>',
    replyTo: 'test',
  },
  result: {error: ValidationError},
}, {
  name: 'should fail due to empty subject',
  payload: {
    to: {name: 'Test', email: 'test@test.com'},
    from: {name: 'Test', email: 'test@test.com'},
    subject: '',
    content: '<h1>TEST</h1>',
  },
  result: {error: ValidationError},
}];

describe('validate email payload', () => {
  test.each(testCases)(
      '$name',
      ({payload, result}) => {
        if (!!result.error) {
          return expect(validateEmailPayload(payload)).rejects.toThrow(result.error);
        } else {
          return expect(validateEmailPayload(payload)).resolves.toBeDefined();
        }
      },
  );
});
