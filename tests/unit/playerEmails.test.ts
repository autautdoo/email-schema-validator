import {validateEmailPayload} from '../../src';
import {ValidationError} from 'joi';

const testCases = [{
  name: 'should pass since both playerId and template are provided',
  payload: {
    playerId: '1',
    template: 'test_template',
    subject: 'test',
    globalMergeVars: [{
      name: 'REGISTRATION_COMPLETEREGURL_TOKEN',
      content: 'https://www.experiencehub.co',
    }],
  },
  result: {error: null},
}, {
  name: 'should pass since both playerId and templateName are provided',
  payload: {
    playerId: '1',
    templateName: 'test_template',
  },
  result: {error: null},
}, {
  name: 'should throw validation error due to missing template and templateName',
  payload: {
    playerId: '1',
  },
  result: {error: ValidationError},
}, {
  name: 'should throw validation error due to missing from email',
  payload: {
    playerId: '1',
    from: {name: 'Test'},
    subject: 'test',
    content: '<h1>TEST</h1>',
  },
  result: {error: ValidationError},
}, {
  name: 'should pass since both playerId, subject, content and from email are provided',
  payload: {
    playerId: '1',
    from: {name: 'Test', email: 'test@test.com'},
    subject: 'test',
    content: '<h1>TEST</h1>',
  },
  result: {error: null},
}, {
  name: 'should pass with language parameter',
  payload: {
    playerId: '1',
    from: {name: 'Test', email: 'test@test.com'},
    subject: 'test',
    language: 'en',
    content: '<h1>TEST</h1>',
  },
  result: {error: null},
}, {
  name: 'should allow playerId as integer',
  payload: {
    playerId: 1,
    from: {name: 'Test', email: 'test@test.com'},
    subject: 'test',
    language: 'en',
    content: '<h1>TEST</h1>',
  },
  result: {error: null},
}, {
  name: 'should fail due to missing subject AND template/templateName',
  payload: {
    playerId: 1,
    from: {name: 'Test', email: 'test@test.com'},
    subject: '',
    language: 'en',
    content: '<h1>TEST</h1>',
  },
  result: {error: ValidationError},
}, {
  name: 'should allow globalMergeVars as empty array',
  payload: {
    playerId: 1,
    from: {name: 'Test', email: 'test@test.com'},
    subject: 'test',
    language: 'en',
    content: '<h1>TEST</h1>',
    globalMergeVars: [],
  },
  result: {error: null},
}, {
  name: 'should allow subject to be null',
  payload: {
    playerId: 1,
    from: {name: 'Test', email: 'test@test.com'},
    subject: null,
    templateName: 'test_template',
    globalMergeVars: [],
  },
  result: {error: null},
}, {
  name: 'should allow subject to be empty string',
  payload: {
    playerId: 1,
    from: {name: 'Test', email: 'test@test.com'},
    subject: '',
    templateName: 'test_template',
    globalMergeVars: [],
  },
  result: {error: null},
}];

describe('validate player email payload', () => {
  test.each(testCases)(
      '$name',
      ({payload, result}) => {
        if (!!result.error) {
          return expect(validateEmailPayload(payload)).rejects.toThrow(result.error);
        } else {
          return expect(validateEmailPayload(payload)).resolves.toBeDefined();
        }
      },
  );
});

